/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

/**
	@author Scott West <westsg2@mcmaster.ca>
*/
class Vector{
 public:
  Vector ();
  Vector(double x, double y, double z);
  Vector(const Vector&);

  ~Vector();
  
  double dot (const Vector& v2) const;
  Vector cross (const Vector& v2) const;
  Vector add (const Vector& v2) const;
  Vector sub (const Vector& v2) const;

  Vector operator+(const Vector& v2) const { return this->add(v2); };
  Vector operator-(const Vector& v2) const { return this->sub(v2); };
  
  Vector scale (double s) const;
  Vector norm () const ;
  double length () const;
    
  double x, y, z;

 private:
  friend std::ostream& operator<<(std::ostream & os, const Vector& v)
    {
      return os << "<" << v.x << "," << v.y << "," << v.z << ">";
    }
};

#endif
