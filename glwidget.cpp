/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "glwidget.h"

#include <btBulletDynamicsCommon.h>

#include <iostream>

#define MOVE_STEP 0.05

GLWidget::GLWidget(QWidget* parent) : QGLWidget(parent)
{  
  setFocus ();
  grabKeyboard ();
  resize (800,600);

  btVector3 worldMin (-100,-100,-100);
  btVector3 worldMax ( 100, 100, 100);
  int maxProxies = 1024;

  btAxisSweep3* broadphase = 
    new btAxisSweep3 (worldMin, worldMax, maxProxies);

  btDefaultCollisionConfiguration* collisionConf = 
    new btDefaultCollisionConfiguration();

  btCollisionDispatcher* dispatcher = 
    new btCollisionDispatcher(collisionConf);

  btSequentialImpulseConstraintSolver* solver = 
    new btSequentialImpulseConstraintSolver;

  btWorld = 
    new btDiscreteDynamicsWorld(dispatcher,broadphase,solver,collisionConf);
  
  btWorld->setGravity(btVector3(0,-0.5,0));
  
  objects.push_back ( new Box ( Vector (0,20,0),
				Vector (1,1,3)
				) );
  objects.push_back (new  Box ( Vector (-3,20,0),
				Vector (1,2,2)
				) );
  objects.push_back (new  Box ( Vector (2,20,0),
				Vector (1,1,2)
				) );

  btWorld->addRigidBody (objects[0]->getBody () );
  btWorld->addRigidBody (objects[1]->getBody () );  
  btWorld->addRigidBody (objects[2]->getBody () );

  btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0,1,0),1);
  btDefaultMotionState* groundMotionState = 
    new btDefaultMotionState(btTransform(btQuaternion (0,0,0,1),
					 btVector3 (0,-1,0)));
  btRigidBody::btRigidBodyConstructionInfo
    groundRigidBodyCI (0,groundMotionState,groundShape,btVector3 (0,0,0));
  btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);
  btWorld->addRigidBody(groundRigidBody);
  
  offsetx = 0.0;
  offsety = 0.0;
  offsetz = 0.0;

}

GLWidget::~GLWidget()
{
}

void GLWidget::keyPressEvent (QKeyEvent *event)
{
  btRigidBody* b = objects[0]->getBody ();

  switch (event->key ())
    {
    case Qt::Key_Left:
      offsetx += MOVE_STEP;
      break;
    case Qt::Key_Right:
      offsetx -= MOVE_STEP;
      break;
    case Qt::Key_Up:
      offsety -= MOVE_STEP;
      break;
    case Qt::Key_Down:
      offsety += MOVE_STEP;
      break;
    case Qt::Key_W:
      offsetz -= MOVE_STEP;
      break;
    case Qt::Key_S:
      offsetz += MOVE_STEP;
      break;
    case Qt::Key_Space:
      b->activate ();
      b->applyForce (btVector3 (0.0, 0.0, 10.0),
		     btVector3 (-3.0, 0.0, 0.0));
      break;
    default:
      break;
    }

  updateGL();
}

void GLWidget::updateGL ()
{
  btWorld->stepSimulation (1/60.0);
  QGLWidget::updateGL ();
}

void GLWidget::paintGL ()
{
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity ();
  
  GLfloat pos_light[] = {-1.0, 2.0, 10.0, 0.0};
  GLfloat dif_light[] = {1.0f, 1.0f, 1.0f, 1.0f};
  glLightfv (GL_LIGHT1, GL_DIFFUSE, dif_light);
  glLightfv (GL_LIGHT1, GL_POSITION, pos_light);

  gluLookAt (offsetx, 10, 20.0,
	     0.0, 0.0, 0.0,
	     0.0, 1.0, 0.0);

  for( std::vector<Box*>::iterator it = objects.begin() ;
       it != objects.end ();
       ++it)
    {
      (*it)->render ();
    }



}

void GLWidget::resizeGL (int w, int h)
{
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glViewport (0, 0, w, h);
  gluPerspective (45.0, (GLfloat)w/(GLfloat)h, 1.0, 500.0);

  glMatrixMode (GL_MODELVIEW);
}

void GLWidget::initializeGL ()
{
  glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
  
  glEnable (GL_DEPTH_TEST);
  glDepthMask (GL_TRUE);

  glDepthFunc (GL_LEQUAL);
  glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT1);
  glShadeModel (GL_SMOOTH);  
}
