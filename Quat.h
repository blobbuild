#ifndef QUAT_H
#define QUAT_H

#include "vector.h"


class Quat
{
 public:
  Quat ();
  Quat (double, Vector);
  Quat (double, double, double, double);
  Quat (double, double, double);

  Quat operator+(const Quat& q2) const;
  Quat operator-(const Quat& q2) const;
  Quat operator*(const Quat& q2) const;
  Quat inv () const;
  Quat conj () const;
  Quat scale (double) const;

  double norm() const;


 private:
  double s;
  Vector v;
};


#endif
