/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "collision.h"
#include "glwidget.h"

#include <QTimer>

#include <iostream>

#define FPS 60.0

Collision::Collision()
{
  glwidget = new GLWidget;
  setCentralWidget (glwidget);
    
  QTimer* timer = new QTimer();
  timer->setInterval (1.0/FPS);
  connect (timer,SIGNAL(timeout(void)),this,SLOT(update(void)));
  timer->start (1.0/FPS);

  resize (800,600);
}

void Collision::update()
{
  glwidget->updateGL ();
}

Collision::~Collision()
{

}

