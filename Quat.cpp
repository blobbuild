#include <math.h>

#include "Quat.h"

Quat::Quat () {}

Quat::Quat(double sc,
		Vector vec)
{
  s = sc;
  v = vec;
}

Quat::Quat(double sc,
	   double x,
	   double y,
	   double z)
{
  Quat (sc, Vector(x,y,z));
}

Quat::Quat(double roll,
	   double pitch,
	   double yaw)
{
  Quat Qr (cos (roll/2),
	   sin (roll/2),
	   0, 
	   0);
  Quat Qp (cos (pitch/2),
	   0, 
	   sin (pitch/2),
	   0);
  Quat Qy (cos (yaw/2),
	   0,
	   0,
	   sin (yaw/2));

  Quat res = Qr * Qp * Qy;

  s = res.s;
  v = res.v;
}

Quat Quat::operator+(const Quat& q2) const
{
  return Quat (s + q2.s, v + q2.v);
}

Quat Quat::operator-(const Quat& q2) const
{
  return Quat (s - q2.s, v - q2.v);
}

Quat Quat::operator*(const Quat& q2) const
{
  return Quat (s * q2.s - v.dot (q2.v),
	       q2.v.scale (s) + v.scale (q2.s) + v.cross (q2.v)
	       );
}

Quat Quat::conj () const
{
  return Quat (s, v.scale (-1));
}

double Quat::norm () const
{
  return s*s + v.dot(v);
}

Quat Quat::scale (double sc) const
{
  return Quat (s, v.scale (sc));
}

Quat Quat::inv () const
{
  return this->conj ().scale (1.0 / this->norm ());
}
