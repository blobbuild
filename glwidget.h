/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "box.h"

#include <vector.h>

#include <QtOpenGL>

#include <QKeyEvent>
#include <QGLWidget>
#include <QObject>


/**
	@author Scott West <westsg2@mcmaster.ca>
*/
class GLWidget : public QGLWidget
{
    
Q_OBJECT
    public:
        GLWidget(QWidget* parent = 0);
        ~GLWidget();
        Box* box;
	void updateGL ();

    protected:
	void keyPressEvent (QKeyEvent*);
        void initializeGL();
        void resizeGL(int w, int h);
        void paintGL();

    private:
	btDiscreteDynamicsWorld* btWorld;
	std::vector< Box* > walls;
	std::vector< Box* > objects;
	double offsetx, offsety,offsetz;
        GLuint object;
};

#endif
