/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "box.h"

#include <iostream>
#include <math.h>


Box::Box (Vector pos, Vector size) : 
  p      (pos), 
  sz     (size)
{
  // FIXME: default mass, add mass option to the constructor later
  btScalar mass = 1;
  // initially no inertia
  btVector3 inertia (0,0,0);

  // the box shape and size
  btBoxShape* btBox = new btBoxShape (btVector3 (sz.x/2, 
						 sz.y/2, 
						 sz.z/2)
				      );

  // this represents the box position and orientation
  btTransform boxInitTrans;
  boxInitTrans.setIdentity ();
  boxInitTrans.setOrigin (btVector3 (p.x, p.y, p.z));

  btDefaultMotionState* btBoxState = 
    new btDefaultMotionState (boxInitTrans);

  // set the shape's moving properties, inertia, mass.
  btBox->calculateLocalInertia (mass, inertia);

  // create a rigid body with the mass, shape, inertia.
  btRigidBody::btRigidBodyConstructionInfo boxCI 
    (mass, btBoxState, btBox, inertia);
  
  btBoxBody = new btRigidBody (boxCI);
  btBoxBody->setSleepingThresholds(btScalar (0.1), btScalar (0.1));
}


Box::~Box ()
{
}

void Box::update(const Box& b, double dt)
{

}

Vector Box::position (){
  return p;
}

void Box::drawQuad ()
{
  glBegin (GL_QUADS);
  
  glNormal3f (0.0f, 0.0f, 1.0f);
  
  glVertex3f (-0.5,  0.5, 0.5);
  glVertex3f ( 0.5,  0.5, 0.5);
  glVertex3f ( 0.5, -0.5, 0.5);
  glVertex3f (-0.5, -0.5, 0.5);  
      
  glEnd ();
}

btRigidBody* Box::getBody ()
{
  return btBoxBody;
}

void Box::drawBox ()
{
  glPushMatrix ();
  
  for(int i = 0; i < 4; i++)
    {
      glRotated (90.0, 0.0, 1.0, 0.0);
      drawQuad ();
    }
  
  glRotated (90.0, 1.0, 0.0, 0.0);
  drawQuad ();
  
  glRotated (180.0, 1.0, 0.0, 0.0);
  drawQuad ();
    
  glPopMatrix ();
}

void Box::render ()
{
  glPushMatrix ();
  
  glColorMaterial (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable (GL_COLOR_MATERIAL);

  glColor3f (1.0f, 0.0f, 0.0f);

  btTransform trans;
  btScalar matrix[16];

  btBoxBody->getMotionState ()->getWorldTransform (trans);
  trans.getOpenGLMatrix (matrix);
  glMultMatrixf (matrix);

  glScaled (sz.x, sz.y, sz.z);
  drawBox ();
  
  glColor3f (1.0f, 1.0f, 1.0f);
  
  glPopMatrix ();
}
