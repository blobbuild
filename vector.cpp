/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "vector.h"

#include <math.h>

Vector::Vector()
{
}

Vector::Vector(double x, double y, double z) : x(x), y(y), z(z)
{
}

Vector::Vector(const Vector& v)
{
  x = v.x;
  y = v.y;
  z = v.z;
}

Vector::~Vector()
{
}

double Vector::dot (const Vector& v2) const
{
  return this->x * v2.x + this->y * v2.y + this->z * v2.z;
}

Vector Vector::cross (const Vector& v2) const
{
  return Vector (this->y * v2.z - this->z * v2.y,
		 this->z * v2.x - this->x * v2.z,
		 this->x * v2.y - this->y * v2.x);
}

Vector Vector::scale (double d) const
{
  Vector v (d*x,d*y,d*z);
  return v;
}

Vector Vector::add (const Vector& v2) const
{
  Vector v (x + v2.x,
	    y + v2.y,
	    z + v2.z);
  return v;
}

Vector Vector::sub (const Vector& v2) const
{
  Vector v = v2.scale(-1);
  return this->add(v);
}

double Vector::length () const
{
  return dot(*this);
}

Vector Vector::norm () const
{
  Vector v (x,y,z);
  return v.scale (1/v.length ());
}
