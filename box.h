/***************************************************************************
 *   Copyright (C) 2008 by Scott West   *
 *   westsg2@mcmaster.ca   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef BOX_H
#define BOX_H

#if defined (__APPLE__) && defined (__MACH__)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <btBulletDynamicsCommon.h>

#include "vector.h"
/**
	@author Scott West <westsg2@mcmaster.ca>
*/
class Box 
{
 public:
  Box (Vector, Vector);
  ~Box();

  btRigidBody* getBody();
  void render ();
  void update (const Box&, double);
  

 private:
  Vector p;
  Vector sz;

  btRigidBody* btBoxBody;
  
  GLuint side;

  GLuint drawList ();
  void drawBox ();
  void drawQuad ();

  Vector size ();
  Vector position ();
};

#endif
